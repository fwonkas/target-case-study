To run tests, download the following into this "bin" directory

https://github.com/mozilla/geckodriver/releases/download/v0.16.1/geckodriver-v0.16.1-macos.tar.gz
http://selenium-release.storage.googleapis.com/3.4/selenium-server-standalone-3.4.0.jar

Be sure to gunzip and un-tar geckodriver.
