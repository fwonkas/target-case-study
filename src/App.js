import React from 'react'
import Showcase from './components/Showcase/'
import Reviews from './components/Reviews/'
import Priceblock from './components/Priceblock/'
import Returns from './components/Returns/'
import Share from './components/Share/'
import Highlights from './components/Highlights/'
import data from '../data/item-data.json'

require('./App.less')

const App = () => {
  return (
    <div id='pdp'>
      {data.CatalogEntryView.map((item, index) => (
        <div className='pdp-item' key={index}>
          <Showcase
            title={item.title}
            primaryImage={item.Images[0].PrimaryImage[0].image}
            alternateImages={item.Images[0].AlternateImages} />
          <div id='product-info'>
            <Priceblock
              purchasingChannelCode={item.purchasingChannelCode}
              offerPrices={item.Offers[0].OfferPrice}
              promos={item.Promotions} />
            <Returns />
            <Share />
            <Highlights
              description={item.ItemDescription} />
          </div>
          <Reviews
            reviews={item.CustomerReview[0]} />
        </div>
    ))}
    </div>
  )
}

export default App
