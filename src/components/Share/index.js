import React, { Component } from 'react'
import Button from '../Button/'

require('./share.less')

export default class Share extends Component {
  addToRegistryClick () {
    window.alert('Add to Registry')
  }

  addToListClick () {
    window.alert('Add to List')
  }

  shareClick () {
    window.alert('Share')
  }

  render () {
    return (
      <div id='share'>
        <Button clickHandler={this.addToRegistryClick} className='add-to-registry'>Add to Registry</Button>
        <Button clickHandler={this.addToListClick} className='add-to-list'>Add to List</Button>
        <Button clickHandler={this.shareClick} className='share'>Share</Button>
      </div>
    )
  }
}
