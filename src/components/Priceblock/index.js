import React, { Component, PropTypes } from 'react'
import Promotions from '../Promotions/'
import Quantity from '../Quantity/'
import PickUpInStore from '../PickUpInStore/'
import AddToCart from '../AddToCart/'

require('./priceblock.less')

export default class Priceblock extends Component {
  constructor (props) {
    super(props)
    this.state = {
      quantity: 1
    }
    this.incrementQuantity = this.incrementQuantity.bind(this)
    this.decrementQuantity = this.decrementQuantity.bind(this)
  }

  incrementQuantity () {
    this.setState({
      quantity: this.state.quantity + 1
    })
  }

  decrementQuantity () {
    if (this.state.quantity > 1) {
      this.setState({
        quantity: this.state.quantity - 1
      })
    }
  }
  render () {
    const showPickUpInStore = this.props.purchasingChannelCode === '0' || this.props.purchasingChannelCode === '2'
    const showAddToCart = this.props.purchasingChannelCode === '0' || this.props.purchasingChannelCode === '1'
    return (
      <div id='priceblock'>
        { this.props.offerPrices.map((item, index) => (
          <div key={index}>
            <h1 key={item}>{ item.formattedPriceValue }</h1>
            <span className='price-qualifier'>{item.priceQualifier}</span>
          </div>
      )) }
        <Promotions
          promos={this.props.promos} />
        <Quantity
          increment={this.incrementQuantity}
          decrement={this.decrementQuantity}
          quantity={this.state.quantity} />
        <div id='purchase'>
          { showPickUpInStore && <PickUpInStore /> }
          { showAddToCart && <AddToCart quantity={this.state.quantity} /> }
        </div>
      </div>
    )
  }
}
Priceblock.propTypes = {
  purchasingChannelCode: PropTypes.string,
  offerPrices: PropTypes.array,
  promos: PropTypes.array
}
