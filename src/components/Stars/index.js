import React, { Component, PropTypes } from 'react'

require('./stars.less')

export default class Stars extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }
  render () {
    const overallRating = []
    for (let x = 0; x < this.props.possible; x++) {
      overallRating[x] = <span key={x} className={(x < this.props.rating ? 'full-star' : 'no-star')} />
    }
    return (
      <span className={this.props.small ? 'small-stars' : ''}>
        {overallRating}
      </span>
    )
  }
}

Stars.propTypes = {
  rating: PropTypes.number.isRequired,
  possible: PropTypes.number.isRequired,
  small: PropTypes.bool
}
