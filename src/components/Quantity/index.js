import React, { Component, PropTypes } from 'react'

require('./quantity.less')

export default class Quantity extends Component {
  render () {
    return (
      <div id='quantity'>
        <span className='quantity-label'>quantity:</span>
        <span className='controls'>
          <button onClick={this.props.decrement} className='quantity-decrement'><span>-</span></button>
          <span className='quantity'>{ this.props.quantity }</span>
          <button onClick={this.props.increment} className='quantity-increment'><span>+</span></button>
        </span>
      </div>
    )
  }
}

Quantity.propTypes = {
  decrement: PropTypes.func.isRequired,
  increment: PropTypes.func.isRequired,
  quantity: PropTypes.number.isRequired
}
