import React, { Component, PropTypes } from 'react'
import Button from '../Button/'

require('./addtocart.less')

export default class AddToCart extends Component {
  constructor (props) {
    super(props)
    this.onClick = this.onClick.bind(this)
  }

  onClick () {
    window.alert(`clicked add to cart with quantity ${this.props.quantity}`)
  }

  render () {
    return (
      <div id='add-to-cart'>
        <Button className='add-to-cart-button' clickHandler={this.onClick}>Add to Cart</Button>
      </div>
    )
  }
}

AddToCart.propTypes = {
  quantity: PropTypes.number.isRequired
}
