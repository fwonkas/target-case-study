import React, { Component, PropTypes } from 'react'

require('./promotions.less')

export default class Promotions extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  render () {
    return (
      <div id='promotions'>
        <ul>
          {this.props.promos.map((promo, index) => (
            <li key={index}>{promo.Description[0].shortDescription}</li>
          ))}
        </ul>
      </div>
    )
  }
}

Promotions.propTypes = {
  promos: PropTypes.array
}
