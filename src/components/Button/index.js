import React, { Component, PropTypes } from 'react'

require('./button.less')

export default class Button extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }

  render () {
    return (
      <button className={this.props.className} onClick={this.props.clickHandler}>
        {this.props.children}
      </button>
    )
  }
}

Button.propTypes = {
  clickHandler: PropTypes.func.isRequired,
  className: PropTypes.string,
  children: PropTypes.node.isRequired
}
