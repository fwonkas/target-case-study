import React, { Component, PropTypes } from 'react'
import Carousel from '../Carousel/'

require('./showcase.less')

export default class Showcase extends Component {
  constructor (props) {
    super(props)
    this.state = {
      currentImage: this.props.primaryImage
    }
    this.onImageClick = this.onImageClick.bind(this)
    this.onMagClick = this.onMagClick.bind(this)
  }

  setActiveImage (image) {
    this.setState({
      currentImage: image
    })
  }

  onImageClick (image) {
    this.setState({
      currentImage: image
    })
  }

  onMagClick (e) {
    window.alert(`magnify ${this.state.currentImage}`)
  }

  render () {
    const images = [{image: this.props.primaryImage}].concat(this.props.alternateImages)
    return (
      <div id='showcase'>
        <h1>{ this.props.title }</h1>
        <img src={this.state.currentImage} />
        <div onClick={this.onMagClick} className='magnify'>
          <img src='/images/mag.png' /> view larger
        </div>
        <Carousel
          clickHandler={this.onImageClick}
          itemHeight={50}
          itemWidth={50}
          activeItemBorder={1}
          itemsPerSlide={3}
          images={images} />
      </div>
    )
  }
}

Showcase.propTypes = {
  title: PropTypes.string,
  alternateImages: PropTypes.array,
  primaryImage: PropTypes.string
}
