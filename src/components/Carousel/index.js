import React, { Component, PropTypes } from 'react'

require('./carousel.less')

export default class Carousel extends Component {
  constructor (props) {
    super(props)
    this.state = {
      activeIndex: 0,
      activeSlide: 0,
      slides: Math.ceil(this.props.images.length / 3)
    }
    this.clickImage = this.clickImage.bind(this)
    this.incrementSlide = this.incrementSlide.bind(this)
    this.decrementSlide = this.decrementSlide.bind(this)
  }

  clickImage (e) {
    this.setState({
      activeIndex: parseInt(e.target.dataset.index)
    })
    this.props.clickHandler(e.target.src)
  }

  decrementSlide () {
    if (this.state.activeSlide > 0) {
      this.setState({
        activeSlide: this.state.activeSlide - 1
      })
    }
  }

  incrementSlide () {
    if (this.state.activeSlide + 1 < this.state.slides) {
      this.setState({
        activeSlide: this.state.activeSlide + 1
      })
    }
  }

  render () {
    const height = `${this.props.itemHeight + this.props.activeItemBorder * 2}px`
    const width = this.props.itemWidth + this.props.activeItemBorder * 2
    const slideWidth = width * this.props.itemsPerSlide
    const sliderStyles = {
      left: `${this.state.activeSlide * -slideWidth}px`,
      height
    }
    const sliderWrapperStyles = {
      width: `${slideWidth}px`,
      height
    }
    const prevButton = this.state.activeSlide === 0
      ? <button disabled>&lt;</button>
      : <button onClick={this.decrementSlide}>&lt;</button>

    const nextButton = this.state.activeSlide === this.state.slides - 1
      ? <button disabled>&gt;</button>
      : <button onClick={this.incrementSlide}>&gt;</button>

    return (
      <div className='carousel'>
        {prevButton}
        <div className='slider-wrapper' style={sliderWrapperStyles}>
          <div style={sliderStyles} className='slider'>
            { this.props.images.map((item, index) => (
              <img className={index === this.state.activeIndex ? 'active' : ''} key={index} data-index={index} onClick={this.clickImage} width={this.props.itemWidth} src={item.image} />
            )) }
          </div>
        </div>
        {nextButton}
      </div>
    )
  }
}

Carousel.propTypes = {
  images: PropTypes.array.isRequired,
  clickHandler: PropTypes.func.isRequired,
  itemHeight: PropTypes.number.isRequired,
  itemWidth: PropTypes.number.isRequired,
  activeItemBorder: PropTypes.number.isRequired,
  itemsPerSlide: PropTypes.number.isRequired
}
