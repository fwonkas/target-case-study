import React, { Component } from 'react'

require('./returns.less')

export default class Returns extends Component {
  render () {
    return (
      <div id='returns'>
        <h1>returns</h1>
        <div>This item must be returned within 30 days of the ship date. See <a href='javascript:void(0)'>return policy</a> for details. Prices, promotions, styles and availability may vary by store and online.</div>
      </div>
    )
  }
}
