import React, { Component, PropTypes } from 'react'

require('./highlights.less')

export default class Highlights extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }
  render () {
    return (
      <div id='highlights'>
        <h2>Product Highlights</h2>
        <ul>
          {this.props.description[0].features.map((feature, index) => (
            <li key={index} dangerouslySetInnerHTML={{__html: feature}} />
          ))}
        </ul>
      </div>
    )
  }
}

Highlights.propTypes = {
  description: PropTypes.array.isRequired
}
