import React, { Component, PropTypes } from 'react'
import Stars from '../Stars/'

require('./reviews.less')

export default class Reviews extends Component {
  constructor (props) {
    super(props)
    this.state = {}
    this.onViewAllClick = this.onViewAllClick.bind(this)
  }

  onViewAllClick () {
    window.alert('view all')
  }

  render () {
    const pro = this.props.reviews.Pro[0]
    const con = this.props.reviews.Con[0]
    const proDate = new Date(pro.datePosted).toLocaleDateString('en-US', {month: 'short', day: 'numeric', year: 'numeric'})
    const conDate = new Date(con.datePosted).toLocaleDateString('en-US', {month: 'short', day: 'numeric', year: 'numeric'})
    const overallStars = parseInt(this.props.reviews.consolidatedOverallRating)
    return (
      <div>
        <div className='review-summary'>
          <div>
            <Stars rating={overallStars} possible={5} /> overall
          </div>
          <div className='review-view-all' onClick={this.onViewAllClick}>
          view all {this.props.reviews.Reviews.length} reviews
          </div>
        </div>
        <div id='reviews'>
          <div className='review-head'>
            <h2>PRO</h2>
            <div>most helpful 4&ndash;5 star review</div>
          </div>
          <div className='review-head'>
            <h2>CON</h2>
            <div>most helpful 1&ndash;2 star review</div>
          </div>
          <div className='review-pro'>
            <div className='review-stars'>
              <Stars rating={parseInt(pro.overallRating)} possible={5} small />
            </div>
            <div className='review-title'>
              {pro.title}
            </div>
            <div className='review-body'>
              {pro.review}
            </div>
            <div className='review-sig'>
              <span className='review-screen-name'>{pro.screenName}</span> {proDate}
            </div>
          </div>
          <div className='review-con'>
            <div className='review-stars'>
              <Stars rating={parseInt(con.overallRating)} possible={5} small />
            </div>
            <div className='review-title'>
              {con.title}
            </div>
            <div className='review-body'>
              {con.review}
            </div>
            <div className='review-sig'>
              <span className='review-screen-name'>{con.screenName}</span> {conDate}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Reviews.propTypes = {
  reviews: PropTypes.object.isRequired
}
