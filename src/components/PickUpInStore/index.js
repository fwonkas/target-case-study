import React, { Component } from 'react'
import Button from '../Button/'

require('./pickupinstore.less')

export default class PickUpInStore extends Component {
  constructor (props) {
    super(props)
    this.state = {}
    this.onClick = this.onClick.bind(this)
    this.onClickFind = this.onClickFind.bind(this)
  }

  onClick () {
    window.alert('clicked pick up in store')
  }

  onClickFind () {
    window.alert('clicked Find in a Store')
  }

  render () {
    return (
      <div id='pick-up-in-store'>
        <Button clickHandler={this.onClick}>Pick Up In Store</Button>
        <div>
          <Button clickHandler={this.onClickFind} className='find-in-a-store'>find in a store</Button>
        </div>
      </div>
    )
  }
}
