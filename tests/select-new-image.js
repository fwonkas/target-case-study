module.exports = {
  'Select Image test': (client) => {
    const thumbnail = '#showcase > div.carousel > div > div > img:nth-child(2)'
    client
      .url('http://localhost:8080/')
      .click(thumbnail)

    client.expect.element('#showcase > img').to.have.attribute('src').which.contains('http://target.scene7.com/is/image/Target/14263758_Alt01')
    client.expect.element(thumbnail).to.have.attribute('class').which.contains('active')
    client.end()
  }
}
