# Continuous delivery

I would follow a pattern similar to git-flow, then use a tool like
jenkins for build and deployment.

![Git Flow Diagram](http://jeffkreeftmeijer.com/images/gitflow.gif)

In brief, normal development should happen off a "develop" branch.
Branches should be cut from it for features, bug fixes, etc. Once
those changes are done and approved, they're merged back into the
develop branch. When it's time for a release, create a release
branch off develop. Hardening and bug fixes should take place in
this branch. Fixes in the release branch should be merged back to
develop. When the release branch has hit a desired state, it is
merged to both develop and a master branch, where it is tagged.
Then it can be tested, built and deployed by jenkins.
